
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
         language="java"%>
<%@ page session="false"%>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Вход</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/regs.css" />

    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
</head>
<body>

<div class="wrapper">
    <form action="#" method="POST" id="form">
        <div id="form-fields-container">
            <ul>
                <li>
                    <label for="name">Логин</label>
                    <input type="text" name="name" id="name">
                    <span></span>
                </li>
                <li>
                    <label for="surname">Имя</label>
                    <input type="text" name="surname" id="surname" value="${login}">
                    <span></span>
                </li>
                <li>
                    <label for="city">Город</label>
                    <input type="text" name="city" id="city">
                    <span></span>
                </li>
            </ul>
            <ul>
                <li>
                    <label for="email">Эл. почта</label>
                    <input type="email" name="email" id="email">
                    <span></span>
                </li>
                <li>
                    <label for="pass">Пароль</label>
                    <input type="password" name="pass" id="pass">
                    <span></span>
                </li>
                <li>
                    <label for="pass-repeat">Повторите</label>
                    <input type="password" name="passrepeat" id="pass-repeat">
                    <span></span>
                </li>
            </ul>



                <input type="submit" id="submit4" value="">
                <a class="btn btn-primary" id="submit" href="j_spring_security_logout"></a>
                <span id="message"></span>




        </div><!-- end form-fields-container -->
        <c:if test="${not empty error}">
            <div class="alert alert-danger" style="width: 285px; margin: 0px auto;" role="alert">
                    ${error}</div>
        </c:if>



        <meta charset="utf-8">
        <title>Фон</title>
        <style>
            body {
                background: #c7b39b url(http://hq-wallpaper.ru/wallpapers/kosmos/v-dalekoy-galaktike.jpg); /* Цвет фона и путь к файлу */
                color: #15F12B;; /* Цвет текста */

            }
        </style>


    </form>
</div><!-- end wrapper -->
</body>
