<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ page session="false" %>
<html>
<head>
	<title>Успешная регистрация</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/regs.css" />

	<link rel="stylesheet"
		  href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
</head>
<body>
<h1>Поздравляем, вы успешно зарегистрированы!</h1>

<p>Теперь вы можете авторизироваться</p>

<a class="btn btn-primary" id="submit6" href="j_spring_security_logout"></a>
<span id="message"></span>


<meta charset="utf-8">
<title>Фон</title>
<style>
	body {
		background: #c7b39b url(http://hq-wallpaper.ru/wallpapers/kosmos/v-dalekoy-galaktike.jpg); /* Цвет фона и путь к файлу */
		color: #15F12B;; /* Цвет текста */

	}
</style>
</body>
</html>
