<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
         language="java" %>
<%@ page session="false" %>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Вход</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/regs.css"/>

    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
</head>
<body onload='document.f.j_username.focus();'>

<div class="wrapper">
    <form name='f' action="<c:url value='j_spring_security_check' />"
          method='POST'>

        <div id="form-fields-container">
            <ul>
                <li>
                    <label for="name">Логин</label>
                    <input type="text" name="j_username" id="name">
                    <span></span>
                </li>
                <li>
                    <label for="surname">Пароль</label>
                    <input type="password" name="j_password" id="surname">
                    <span></span>
                </li>
            </ul>



            <input class="btn btn-primary" type="submit" id="submit2" value=""/>
            <input type="button" id="submit3" value="" onClick='location.href="http://localhost:8080/reg"'>
            <%--<a class="btn btn-primary" id="submit2" name="submit" type="submit" value="Вход" ></a>--%>


            <span id="message"></span>
        </div>


<!-- end form-fields-container -->


<meta charset="utf-8">
<title>Фон</title>
<style>

    body {
        background: #c7b39b url(http://hq-wallpaper.ru/wallpapers/kosmos/v-dalekoy-galaktike.jpg); /* Цвет фона и путь к файлу */
        color: #15F12B; /* Цвет текста */

    }
</style>
</head>

<c:if test="${not empty error}">
    <div class="alert alert-danger" style="width: 285px; margin: 0px auto;" role="alert">
            ${error}</div>
</c:if>


</form>
</div><!-- end wrapper -->
</body>




