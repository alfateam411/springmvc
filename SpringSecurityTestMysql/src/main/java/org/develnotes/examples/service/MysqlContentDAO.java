package org.develnotes.examples.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Service
public class MysqlContentDAO {
	
	private final Logger log = LoggerFactory.getLogger(MysqlContentDAO.class);
	@Autowired
	private DataSource dataSource;

	public String getUserPasswordHash(String username) {
		
		String sql = "select pwdHash from users where login=?;";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				return rs.getString("pwdHash");
			}
			rs.close();
			ps.close();
			
		} catch (Exception e){
			log.error("Error  : " + e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {}
			}
		}
			
		return null;
	}

	/**
	 * Получить роль пользователя по имени пользователя
	 * @param username имя пользователя
	 * @return роль пользователя
	 */
	public String getUserRole(String username) {

		String roleName = "";

		String sql = "select userRole from users where login=?;";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				roleName = rs.getString("userRole");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			log.error("Error  : " + e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}

		return roleName;
	}

}
