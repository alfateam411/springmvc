package org.develnotes.examples.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;


@Service
public class MysqlUserService implements UserDetailsService, Serializable {

	public String login;
	private static final long serialVersionUID = -7858305254948955717L; 
	@Autowired
	private MysqlContentDAO contentService;
	private static final Logger logger = LoggerFactory.getLogger(MysqlUserService.class);

	@Override
	public UserDetails loadUserByUsername(final String username) {

		//создаем простой анонимный класс, реализующий интерфейс UserDetails
		return new UserDetails() {

			private static final long serialVersionUID = 2059202961588104658L;

			@Override
			public boolean isEnabled() {
				return true;
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return true;
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}
			
			@Override
			public String getUsername() {
				login = username;
				return username;
			}

			@Override
			public String getPassword() {

				try {
					
					//получаем хэш пароля по имени пользователя
					return contentService.getUserPasswordHash(username);
					
				} catch (Exception e) {
					logger.error("Error while access MYSQL database to get pwdHash for user = "
							+ username);
				}
				return null;
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				List<SimpleGrantedAuthority> auths = new java.util.ArrayList<SimpleGrantedAuthority>();

				try {
					//получаем роль пользователя по имени пользователя
					auths.add(new SimpleGrantedAuthority(contentService.getUserRole(username)));
					
				} catch (Exception e) {
					logger.error("Error while access MYSQL database "
							+ "to get details for user = " + username);
				}

				return auths;
			}
		};
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public MysqlContentDAO getContentService() {
		return contentService;
	}
	
	public void setContentService(MysqlContentDAO contentService) {
		this.contentService = contentService;
	}
}
