package org.develnotes.examples;

import org.develnotes.examples.form.SignupForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

/**
 * Created by Павел on 05.04.2016.
 */
@Controller
@RequestMapping("/reg")
public class RegestrationController {

    private final String ERROR_MESSAGE = "Логин должен быть больше 3, но меньше 16 символов";
    private final String ERROR_MESSAGE1 = "Пароль должен быть больше 5, но меньше 25 символов";
    private final String ERROR_MESSAGE2 = "При попытке ввести пароль повторно, вы ввели неверно";
    private final String ERROR_MESSAGE3 = "Такой логин уже существует";
    private final String ERROR_MESSAGE4 = "Введите почту";

/*@Autowired
private SignupValidator signupValidator;*/

    @RequestMapping(method = RequestMethod.GET)
    public String signup(ModelMap model) {
        SignupForm signupForm = new SignupForm();
        model.put("signupForm", signupForm);
        return "reg";
    }


    @RequestMapping(method = RequestMethod.POST)
    public String processSignup(SignupForm signupForm, BindingResult result,ModelMap model ) {
        String shifr = "";
        String name1 = signupForm.getName();

//signupValidator.validate(signupForm, result);
        if (!(name1.length() > 3 && name1.length() < 16)) {
            model.addAttribute("error", ERROR_MESSAGE);
            return "reg";
        }

        if (signupForm.getEmail().length() == 0) {
            model.addAttribute("error", ERROR_MESSAGE4);
            return "reg";
        }

        if (!(signupForm.getPass().length() > 5 && signupForm.getPass().length() < 25)) {
            model.addAttribute("error", ERROR_MESSAGE1);
            return "reg";
        }

        if (!signupForm.getPass().equals(signupForm.getPassrepeat())) {
            model.addAttribute("error", ERROR_MESSAGE2);
            return "reg";
        }

/////////////////////
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test_spring","spring_app", "123456");
            Statement statement = conn.createStatement();
// считывание из базы
// запрос к базе
            String zapros = "select * from users";
// выбираем данные с БД
            ResultSet rs = statement.executeQuery(zapros);
// И если что то было получено то цикл while сработает
            while (rs.next()) {
// считываем
                String login = rs.getString("login");
                System.out.println(login);
                if (signupForm.getName().equals(login)) {
                    model.addAttribute("error", ERROR_MESSAGE3);
                    return "reg";
                }
            }

// тест >
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Где драйвер?");
                e.printStackTrace();
                System.out.println("SDSD");
                return "reg";
            }

            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update((signupForm.getPassrepeat()).getBytes("UTF-8"));
            shifr = new BigInteger(1, crypt.digest()).toString(16);
            System.out.println(shifr);
// добавление записей в базу
            statement.executeUpdate("INSERT INTO users (login, userRole, name, city, email, pwdHash) " +
                    "values('"+signupForm.getName()+"', 'ROLE_ADMIN','"+signupForm.getSurname()+"', '"+signupForm.getCity()+"', '"+signupForm.getEmail()+"', '"+shifr+"')");

            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "signup-success";
    }
}