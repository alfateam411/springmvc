package org.develnotes.examples.form;

/**
 * Created by Павел on 08.04.2016.
 */
public class MainForm {
    private String name;
    private String surname;
    private String city;
    private String pass;
    private String passrepeat;
    private String email;

    public MainForm() {
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setPassrepeat(String passrepeat) {
        this.passrepeat = passrepeat;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getPassrepeat() {
        return passrepeat;
    }

    public String getPass() {
        return pass;
    }

    public String getEmail() {
        return email;
    }
}
