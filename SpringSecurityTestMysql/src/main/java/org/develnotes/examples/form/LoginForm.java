package org.develnotes.examples.form;

/**
 * Created by Павел on 06.04.2016.
 */
public class LoginForm {
    private String username;
    private String password;

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
