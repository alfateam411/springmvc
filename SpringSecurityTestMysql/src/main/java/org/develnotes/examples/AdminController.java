package org.develnotes.examples;

import org.develnotes.examples.form.MainForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Павел on 05.04.2016.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

	@RequestMapping(method = RequestMethod.GET)
	public String signup(ModelMap model) {
		MainForm mainForm = new MainForm();


		model.put("mainform", mainForm);
		model.addAttribute("up1", "Меняет местами 1 со 2, 3 с 4");
		model.addAttribute("up2", "Шифр Цезаря со сдвигом на 3 (для русского текста и цифр");
		model.addAttribute("up3", "Md5");
		return "private/admin";
	}


	@RequestMapping(method = RequestMethod.POST)
	public String processSignup(MainForm mainForm, BindingResult result,ModelMap model ) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		model.addAttribute("up1", "Меняет местами 1 со 2, 3 с 4");
		model.addAttribute("up2", "Шифр Цезаря со сдвигом на 3 (для русского текста и цифр");
		model.addAttribute("up3", "Md5");

		//1 шифр
        String text1 = mainForm.getName();
		String newtext1 = "";
		String shifr1 = "";
		for (int i = 0; i<= text1.length() - 1; i++ )
			if (text1.substring(i,i+1).equals(",")) {
				i = text1.length();
			}
			else {
				newtext1 = newtext1 + text1.substring(i,i+1);
			}
		if (!newtext1.equals("")) {
			if (newtext1.length() > 1) {
				for (int i = 0; i <= newtext1.length() - 1; i=i+2) {
					if (i == newtext1.length() - 1) {
						shifr1 = shifr1 + newtext1.substring(i, i+1);
					} else {
						shifr1 = shifr1 + newtext1.substring(i+1, i+2) + newtext1.substring(i, i+1);
					}

				}
			}
		}
		model.addAttribute("text1", newtext1);
		model.addAttribute("shif1", shifr1.toLowerCase());

		//2 шифр
		String alfavit[]= {"а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р",
				"с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", "1", "2", "3", "4", "5",
				"6", "7", "8", "9", "0"};
		String text2 = mainForm.getSurname();
		String newtext2 = "";
		String shifr2 = "";
		for (int i = 0; i<= text2.length() - 1; i++ )
			if (text2.substring(i,i+1).equals(",")) {
				i = text2.length();
			}
			else {
				newtext2 = newtext2 + text2.substring(i,i+1).toLowerCase();
			}

		if (!newtext2.equals("")) {
			if (newtext2.length() > 1) {
				for (int i = 0; i <= newtext2.length() - 1; i=i+1) {
					for (int k = 0; k <= alfavit.length - 1; k++) {
						if (alfavit[k].equals(newtext2.substring(i, i+1))) {
							if (k > alfavit.length - 4) {
								shifr2 = shifr2 + alfavit[(k+3) % (alfavit.length)];
								System.out.println(shifr2);
							}
							else {
								shifr2 = shifr2 + alfavit[k+3];
							}
						}
					}
				}
			}
		}
		model.addAttribute("text2", newtext2);
		model.addAttribute("shif2", shifr2.toLowerCase());

		//3 шифр
		String text3 = mainForm.getCity();
		String newtext3 = "";
		for (int i = 0; i<= text3.length() - 1; i++ )
			if (text3.substring(i,i+1).equals(",")) {
				i = text3.length();
			}
			else {
				newtext3 = newtext3 + text3.substring(i,i+1).toLowerCase();
			}

		String md5Hex = "";
		if (!newtext3.equals("")) {
			if (newtext3.length() > 1) {
				MessageDigest messageDigest = null;
				byte[] digest = new byte[0];

				try {
					messageDigest = MessageDigest.getInstance("MD5");
					messageDigest.reset();
					messageDigest.update(newtext3.getBytes());
					digest = messageDigest.digest();
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}

				BigInteger bigInt = new BigInteger(1, digest);
				md5Hex = bigInt.toString(16);

				while (md5Hex.length() < 32) {
					md5Hex = "0" + md5Hex;
				}
			}
		}
		model.addAttribute("text3", newtext3);
		model.addAttribute("shif3", md5Hex);
		return "private/admin";
	}
}